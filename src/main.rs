use std::env;
use std::process;

use structopt::StructOpt;
use minigrep::Config;

#[macro_use]
extern crate clap;
use clap::App;


fn main() {

    let args = Config::from_args();

    if let Err(e) = minigrep::run(args) {
        eprintln!("Application error: {}", e);

        process::exit(1);
    

    }
}
